import sys
from copy import deepcopy
from random import sample
from linecache import getline

def constructModel(trainingFilepath, modelFilepath, devFilepath):
    try:
        trainingFile = open(trainingFilepath, mode='r')
        modelFile = open(modelFilepath, mode='w')
        
        NUMBER_OF_ITERATIONS = 30
        
        weightVectors = {} #key = word, value = dictionary of key = class, value = weight
        cachedWeightVectors = {}
        avgWeightVectors = {}
        vocabulary = {} #key = word, value = dummy variable (0)
        
        z = {} #key=class name, value = intermediate z values in each training sample
             
        sortedClasses = []
              
        #repeat for N iterations
        iterationNumber = 0
        
        counter = 1.0
        
        #min error rate and corresponding avg weight vector among all iterations
        minErrorRate = 1.0
        minAvgWeightVectors = {}
        
        #if no reduction in error rate for 5 iterations, stop iterating
        iterationsSinceLastDecrease = 0
        
        while iterationNumber < NUMBER_OF_ITERATIONS:
            
            iterationNumber += 1
            
            randomLineNums = getRandomLineNums(trainingFile)          
                        
            #process training file, random order
            for lineNumber in randomLineNums:
                
                features = getline(trainingFilepath, lineNumber).strip().split()
                
                label = features[0]           
                
                del features[0] #remove label from consideration
                
                #new class being encountered in traininf data
                if(label not in sortedClasses):
                    sortedClasses.append(label)
                    sortedClasses = sorted(sortedClasses)
                    
                    #also add new class to each weight vector
                    for key in weightVectors:
                        weightVectors[key][label] = 0.0
                    for key in cachedWeightVectors:
                        cachedWeightVectors[key][label] = 0.0
                    for key in avgWeightVectors:
                        avgWeightVectors[key][label] = 0.0
                
                #******** RE-INITIALIZE Z ********
                for className in sortedClasses:
                        z[className] = 0.0  
                
                #******** FIND Z FOR EACH CLASS FOR THE PARTICULAR EXAMPLE ********
                for feature in features: #iterate over all features in a sample                      
                    if(iterationNumber == 1): #in subsequent iterations, weight vectors will have all tokens in vocabulary                             
                        if(feature not in vocabulary): #if feature does not exist in vocabulary, add to all weight vectors
                            vocabulary[feature] = 0
                            weightVectors[feature] = {}
                            cachedWeightVectors[feature] = {}
                            avgWeightVectors[feature] = {}
                            for className in sortedClasses:
                                weightVectors[feature][className] = 0.0
                                cachedWeightVectors[feature][className] = 0.0
                                avgWeightVectors[feature][className] = 0.0
                    
                    for className in sortedClasses:
                        z[className] += weightVectors[feature][className]
                

                #******** FIND ARGMAX ********
                z_pred_class = sortedClasses[0]
                z_max = 0.0
                for className in sortedClasses:
                    if(z[className] > z_max):
                        z_pred_class = className
                        z_max = z[className]
                
                
                #******** IF WRONG, THEN UPDATE WEIGHTS ********
                if(z_pred_class != label):
                    for feature in features:
                        weightVectors[feature][z_pred_class] -= 1 #decrease the wrong ans by -1 for each feature
                        weightVectors[feature][label] += 1 #increase the correct ans by +1 for each feature
                        
                        #avg weight vectors  u <-- u + y*c*x
                        cachedWeightVectors[feature][z_pred_class] -= counter*1
                        cachedWeightVectors[feature][label] += counter*1                        
                
                counter += 1.0
            
            #******** FIND AVG WEIGHT VECTORS avgweight = c*w - u AND AVG BIAS = c*b - beta ********
            for className in sortedClasses:    
                for token in vocabulary:
                    avgWeightVectors[token][className] = counter*weightVectors[token][className] - cachedWeightVectors[token][className]
                    
                        
            #1 iteration is over and our avg weight vector is ready, try to classify dev data and test its performance, if available
            print("Iteration {}".format(iterationNumber), end="")
            if(devFilepath != ""):
                errorRate = tryOnDevData(devFilepath, avgWeightVectors)
                
                if(errorRate < minErrorRate):
                    minErrorRate = errorRate
                    minAvgWeightVectors = deepcopy(avgWeightVectors)                    
                    iterationsSinceLastDecrease = 0
                else:
                    iterationsSinceLastDecrease += 1
                
                if(iterationsSinceLastDecrease == 8): #no decrease in error rate since the last 5 iterations, stop iterating
                    break
                    
            else:
                minAvgWeightVectors = avgWeightVectors
                print()  
        
        
        if(devFilepath != ""):
            print("\nUsing Min Avg. weight vector: {}\n".format(minErrorRate))
            tryOnDevData(devFilepath, minAvgWeightVectors)
        print()    
                    
        #******** Write model to file ********        
        for className in sortedClasses: #names of all classes, sorted order
            modelFile.write("{} ".format(className))
        
        modelFile.write("\n")
        
        modelFile.write("{}\n".format(len(vocabulary))) #size of vocabulary
        
        for token in vocabulary: #print entire vocabulary with corresponding weight vector value, for each class
            modelFile.write("{} ".format(token))
            for className in sortedClasses:
                modelFile.write("{} ".format(minAvgWeightVectors[token][className]))   
            modelFile.write("\n")              
            
        trainingFile.close()
        modelFile.close()
    except FileNotFoundError as e:
        print("File not found. Error: {}".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))


def getRandomLineNums(file):
    totalLines = 0
    file.seek(0)
    
    #find total number of lines in file
    while(True):
        try:
            next(file)
            totalLines += 1
        except StopIteration:
            break
    
    #fill a list with random numbers 
    randomLineNumbers = sample(range(1, totalLines+1), totalLines)
        
    return randomLineNumbers
    

def tryOnDevData(devFilepath, avgWeightVectors):
    try:
        devFile = open(devFilepath, mode="r")
        
        
        #sampleFile = open("errors", mode="w")
        
        
        numOfWrongPredictions = 0.0
        sizeOfDevSet = 0.0
        
        classes = []
        for key in avgWeightVectors:
            classes = sorted(avgWeightVectors[key].keys())
            break        
        
        z = {} #predictions dictionary
        
        for className in classes:
            z[className] = 0.0
        
        for testSample in devFile:
            
            sizeOfDevSet += 1.0
            
            features = testSample.split()
            
            label = features[0] #correct answer
            
            del features[0]          
            
            #reinitialize z values
            for className in classes:
                z[className] = 0.0
            
            #Find values of z for each class
            for feature in features:                                
                for className in classes:
                    if(feature not in avgWeightVectors): #unknown word encountered                                   
                        z[className] += 0.0
                    else:
                        z[className] += avgWeightVectors[feature][className]
                    
            #Find argmax
            z_pred_class = classes[0]
            z_max = 0.0
            for className in classes:
                if(z[className] > z_max):
                    z_pred_class = className
                    z_max = z[className]
            
            if(z_pred_class != label): #wrong prediction
                #sampleFile.write("Token: -->{}<-- : Correct: {} Prediction: {}\n".format(testSample.strip(), label, z_pred_class))
                numOfWrongPredictions += 1.0
        
        errorRate = numOfWrongPredictions/sizeOfDevSet
        
        print(" Error Rate: {}/{} = {}, Accuracy: {} %".format(numOfWrongPredictions, sizeOfDevSet, numOfWrongPredictions/sizeOfDevSet, (1-errorRate)*100.0))
            
        #sampleFile.close()    
        devFile.close()
        
        return errorRate
    except FileNotFoundError:
        print("Could not open dev file '{}'".format(devFilepath))
    except BaseException as e:
        print("Unexpected error occurred while trying to classify dev file: {}".format(e))

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 2):
        print("One or more arguments are missing. The correct syntax is \npython3 perceplearn.py <trainingFile> <modelFile> [-h <devFile>]")
    elif(numOfArgs > 4):
        print("Too many arguments. The correct syntax is \npython3 perceplearn.py <trainingFile> <modelFile> [-h <devFile>]")
    else:
        trainingFilepath = ""
        modelFilepath = ""
        devFilepath = ""
        
        indexOfh = -1
        
        try:
            indexOfh = sys.argv.index("-h")
        except ValueError:
            indexOfh = -1
        
        if(indexOfh == -1): #-h not provided
            if(numOfArgs > 2):
                print("Malformed command. The correct syntax is \npython3 perceplearn.py <trainingFile> <modelFile> [-h <devFile>]")          
                return                
            
            trainingFilepath = sys.argv[1]
            modelFilepath = sys.argv[2]
        else:
            try:
                if(indexOfh == 1):
                    devFilepath = sys.argv[2]
                    trainingFilepath = sys.argv[3]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 2):
                    devFilepath = sys.argv[3]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 3):
                    devFilepath = sys.argv[4]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[2]
            except IndexError:
                print("Malformed command. The correct syntax is \npython3 perceplearn.py <trainingFile> <modelFile> [-h <devFile>]")          
                return
        
        constructModel(trainingFilepath, modelFilepath, devFilepath)
    
if __name__ == "__main__":main()