import sys
import codecs

def classify(modelFilepath):
    
    try:  
        modelFile = open(modelFilepath, mode="r")
        
        lineNumber = 0.0
        
        #model parameters
        sortedClasses = []
        sizeOfVocab = 0.0
        avgWeightVectors = {}
        
        for line in modelFile:
            
            lineNumber += 1
            
            if(lineNumber == 1): #1st line in model file contains class names in sorted order
                sortedClasses = line.strip().split()
                
                #initialize weight vectors
                for className in sortedClasses:
                    avgWeightVectors[className] = {}
            
            elif(lineNumber == 2): #2nd line contains size of vocabulary
                sizeOfVocab = int(line.strip())
            
            else:
                parameters = line.strip().split()
                
                token = parameters[0]              
                
                i = 1
                for className in sortedClasses:
                    avgWeightVectors[className][token] = float(parameters[i])       
                    i += 1
        
        #finished reading model file
        modelFile.close()
                    
        if(len(avgWeightVectors[sortedClasses[0]].keys()) != sizeOfVocab):
            print("Model file is corrupted")
            return  
        
        #start reading line by line from stdin
        sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore') #ignore any decoding errors
        inputLine = sys.stdin.readline()
        while(len(inputLine.strip())>0):
            
            z = {} #predictions dictionary        
            for className in sortedClasses:
                z[className] = 0.0
            
            features = inputLine.split()
            
            for feature in features: #predict one line at a time
                
                for className in sortedClasses:
                    if(feature not in avgWeightVectors[className]): #unknown word encountered                                   
                        z[className] += 0.0
                    else:
                        z[className] += avgWeightVectors[className][feature]
                        
            #Find argmax
            z_pred_class = sortedClasses[0]
            z_max = 0.0
            for className in sortedClasses:
                if(z[className] > z_max):
                    z_pred_class = className
                    z_max = z[className]
            
            #write output to stdout and flush it to output
            print(z_pred_class, flush=True)
                
            #read in next line
            inputLine = sys.stdin.readline()     
            
    except FileNotFoundError:
        print("Unable to open file '{}'".format(modelFilepath))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))      

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 1):
        print("One or more arguments are missing. The correct syntax is \npython3 percepclassify.py <modelFile>")
    elif(numOfArgs > 1):
        print("Too many arguments. The correct syntax is \npython3 percepclassify.py <modelFile>")
    else:
        modelFilepath = sys.argv[1] #0th position contains source file name.
        
        classify(modelFilepath)

if __name__ == '__main__': main()