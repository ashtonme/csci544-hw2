***
# A general purpose, multiclass perceptron, used to perform Part-of-Speech tagging and Named entity recognition.
***

### About

This is an implementation of linear classification using a perceptron. The perceptron learns to classify based on the training set and constructs a model, which consists of averaged weight vectors, to be used for classification on unknown data.
While learning, 25 iterations are performed over the training set, during which the perceptron repeatedly tries to improve its performance, averaging the weights along the way. Performance after each iteration is measured on a development set. The average weight vector which performed the best, is chosen at the end of the learning process. If no development set is provided, the average weight vector at the end of the 25th iteration is chosen as the one to be used for classification.
The perceptron requires as input, a document formatted in a specific way (described below), containing tagged training examples. To demonstrate the performance of the perceptron, it is used to perform two classification tasks - Part-of-Speech tagging and Named entity recognition (also included in this repository). Results obtained are documented below.
***

### Part I - The averaged perceptron classifier

The training program is called `perceplearn.py`. Its usage is as follows:

`python3 perceplearn.py <trainingFile> <modelFile> [-h <devFile>]`

<trainingFile> is the path of the file containing the training examples. This file should be in the following format:

LABEL FEATURE1 FEATURE2 FEATURE3...

LABEL FEATURE1 FEATURE2 FEATURE3...

.

.

.

LABEL FEATURE1 FEATURE2 FEATURE3...


Each line represents one training example. The first word in a line represents the class to which that example belongs, followed by the feature-set, separated by spaces in between. The number of lines in the training file corresponds to the number of examples in the training set.
<modelFile> is the path of the model file which will be created as a result of learning. The -h option is used to specify the development data, also formatted in the same way as the training file, and <devFile> is its path.
The model constructed by `perceplearn.py` can now be given as input to `percepclassify.py` to perform classification on unknown data.

The classification program is called `percepclassify.py`. Its usage is as follows:

`python3 percepclassify.py <modelFile>`

<modelFile> is the model constructed by running `perceplearn.py`. `percepclassify.py` reads from standard input, performs classification using the model provided and writes to standard output. Output will be in the following format:

PREDICTED_LABEL

PREDICTED_LABEL

PREDICTED_LABEL

.

.

.

for each line of input.
***

### Part II - using the averaged perceptron to perform Part-Of-Speech tagging

This is done in two stages - the learning phase and the classification phase. `postrain.py` represents the learning phase and `postag.py` represents the classification phase.
Both are present in the postagging folder of the repository.

Learning phase usage:

`postrain.py <trainingFile> <modelFile> [-h <devFile>]`

Arguments have the same meaning as those for `perceplearn.py`, except that <trainingFile> and <devFile> are formatted differently. The format is as follows:

word1/POStag word2/POStag2 word3/POStag3 ... 

word1/POStag word2/POStag2 word3/POStag3 ...

.

.

.

where each line in the training file represents one sentence, and each word is tagged (word and its tag are separated using the '/' symbol)

Classification phase usage:

`python3 postag.py <posModelFile>`

Similar to `percepclassify.py`, `postag.py` reads from standard input, performs classification using the model provided and writes to standard output. Output will be in the following format:

word1/predictedTag word2/predictedTag word3/predictedTag ... 

word1/predictedTag word2/predictedTag word3/predictedTag ...

.

.

.

where each line in the output corresponds to each sentence in input, with the words now tagged.
***

### Part III - using the averaged perceptron to perform Named entity recognition

This is done in two stages - the learning phase and the classification phase. `nelearn.py` represents the learning phase and `netag.py` represents the classification phase.
Both are present in the ner folder of the repository. BIO encoding is used for the NER tags.

Learning phase usage:

`nelearn.py <trainingFile> <modelFile> [-h <devFile>]`

Arguments have the same meaning as those for `postrain.py`, except that <trainingFile> and <devFile> are formatted differently. The format is as follows:

word1/POStag/NERtag1 word2/POStag2/NERtag2 word3/POStag3/NERtag3 ... 

word1/POStag/NERtag1 word2/POStag2/NERtag2 word3/POStag3/NERtag3 ...

.

.

.

where each line in the training file represents one sentence, and each word is tagged with POS tag as well as NER tag (word and its tags are separated using the '/' symbols)

Classification phase usage:

`python3 netag.py <posModelFile>`

Similar to `postag.py`, `netag.py` reads from standard input, performs classification using the model provided and writes to standard output. Output will be in the following format:

word1/POStag/predictedTag word2/POStag2/predictedTag word3/POStag3/predictedTag ... 

word1/POStag/predictedTag word2/POStag2/predictedTag word3/POStag3/predictedTag ...

.

.

.

where each line in the output corresponds to each sentence in input, with the words now tagged with the NER labels.
***

#### What is the accuracy of your part-of-speech tagger?

After training the perceptron on the training data, using the dev data to evaluate performance after each iteration, and later using the model to perform part of speech tagging on the dev data,
the following results were obtained

For JJ: 
Precision: 0.9090546469884324, Recall: 0.9219255663430421, F-score: 0.9154448684474794

For NNS: 
Precision: 0.9835209003215434, Recall: 0.9772364217252396, F-score: 0.9803685897435896

For IN: 
Precision: 0.965768390386016, Recall: 0.9880774962742176, F-score: 0.9767955801104973

For DT: 
Precision: 0.9985611510791367, Recall: 0.9883224152663059, F-score: 0.9934154022330375

For NNP: 
Precision: 0.9723811893505847, Recall: 0.9666089537472174, F-score: 0.9694864797816919

For CC: 
Precision: 0.996, Recall: 0.996, F-score: 0.996

For VBD: 
Precision: 0.9673518742442564, Recall: 0.9384164222873901, F-score: 0.9526644834772253

For NN: 
Precision: 0.9642669469259064, Recall: 0.9615720524017467, F-score: 0.9629176141332867

For WDT: 
Precision: 0.9114583333333334, Recall: 0.9668508287292817, F-score: 0.9383378016085792

For MD: 
Precision: 0.9772727272727273, Recall: 1.0, F-score: 0.9885057471264368

For VB: 
Precision: 0.9586776859504132, Recall: 0.9440488301119023, F-score: 0.9513070220399795

For WRB: 
Precision: 1.0, Recall: 0.9886363636363636, F-score: 0.9942857142857142

For ,: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For VBG: 
Precision: 0.8996598639455783, Recall: 0.8996598639455783, F-score: 0.8996598639455783

For TO: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For POS: 
Precision: 0.9725400457665904, Recall: 0.9929906542056075, F-score: 0.9826589595375723

For .: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For WP$: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For VBP: 
Precision: 0.8920454545454546, Recall: 0.9208211143695014, F-score: 0.9062049062049061

For -LRB-: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For -RRB-: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For RB: 
Precision: 0.9323786793953859, Recall: 0.8960244648318043, F-score: 0.9138401559454191

For VBN: 
Precision: 0.8712753277711561, Recall: 0.86, F-score: 0.8656009473060983

For VBZ: 
Precision: 0.9742857142857143, Recall: 0.9673758865248226, F-score: 0.9708185053380782

For ``: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For '': 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For RBR: 
Precision: 0.7972972972972973, Recall: 0.5619047619047619, F-score: 0.659217877094972

For $: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For CD: 
Precision: 0.9956663055254604, Recall: 0.9962059620596206, F-score: 0.9959360606881604

For JJR: 
Precision: 0.7748344370860927, Recall: 0.8863636363636364, F-score: 0.8268551236749115

For RP: 
Precision: 0.753968253968254, Recall: 0.7421875, F-score: 0.7480314960629921

For PRP: 
Precision: 0.9967266775777414, Recall: 0.9950980392156863, F-score: 0.9959116925592805

For :: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For PRP$: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For PDT: 
Precision: 0.2222222222222222, Recall: 1.0, F-score: 0.3636363636363636

For RBS: 
Precision: 0.8333333333333334, Recall: 0.7894736842105263, F-score: 0.8108108108108109

For WP: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For JJS: 
Precision: 0.9431818181818182, Recall: 0.9540229885057471, F-score: 0.9485714285714286

For FW: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For LS: 
Precision: 1.0, Recall: 0.16666666666666666, F-score: 0.2857142857142857

For UH: 
Precision: 1.0, Recall: 0.2, F-score: 0.33333333333333337

For EX: 
Precision: 0.9411764705882353, Recall: 1.0, F-score: 0.9696969696969697

For NNPS: 
Precision: 0.014925373134328358, Recall: 1.0, F-score: 0.029411764705882353

For #: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

Wrong classifications: 1353 / 40117

Percentage of correct classifications: 96.62736495749932 %


***

#### What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score?

After training the perceptron on the training data, using the dev data to evaluate performance after each iteration, and later using the model to perform named entity recognition on the dev data,
the following results were obtained

For LOC: 
Precision: 0.6749555950266429, Recall: 0.7723577235772358, F-score: 0.7203791469194313

For ORG: 
Precision: 0.759525296689569, Recall: 0.7152941176470589, F-score: 0.7367464404725842

For PER: 
Precision: 0.8169014084507042, Recall: 0.806873977086743, F-score: 0.8118567311650885

For MISC: 
Precision: 0.5578034682080925, Recall: 0.4337078651685393, F-score: 0.4879898862199747

Overall:

Wrong classifications: 1196 / 4351

Percentage of correct classifications: 72.51206619168006 %

Overall Precision: 0.737149533
Overall Recall: 0.725120662
Overall F-score: 0.731085622

***

#### What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is?

The Naive Bayes classifier has an independence assumption, i.e each feature is assumed to be independent of all other features. For a part of speech tagger or a 
named entity recognizer to perform well, it is important to keep context into consideration, i.e features are not independent of one another. Hence, the Naive Bayes classifier
performs comparatively worse than our perceptron when it comes to part of speech tagging and named entity recognition. Here are the results obtained:

##### FOR Named entity recognition:

For LOC: 
Precision: 0.4753289473684211, Recall: 0.5873983739837398, F-score: 0.5254545454545455

For ORG: 
Precision: 0.39155600953353764, Recall: 0.6764705882352942, F-score: 0.4960103515203795

For PER: 
Precision: 0.7486095661846496, Recall: 0.5507364975450082, F-score: 0.6346063177746346

For MISC: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

Wrong classifications: 1950 / 4351

Percentage of correct classifications: 55.18271661686968 %

Overall precision: 0.46876220226474036
Overall recall: 0.5518271661686969
Overall f-score: 0.5069143882613744



##### FOR part of speech tagging:

For JJ: 
Precision: 0.8675128917096391, Recall: 0.8847087378640777, F-score: 0.8760264370118165

For NNS: 
Precision: 0.9702691844114102, Recall: 0.9644568690095847, F-score: 0.9673542960144201

For IN: 
Precision: 0.9268235842460965, Recall: 0.9878291107799304, F-score: 0.9563544547312733

For DT: 
Precision: 0.9928815489749431, Recall: 0.9931643406436913, F-score: 0.9930229246760645

For NNP: 
Precision: 0.9076469253460404, Recall: 0.9893643334157803, F-score: 0.9467455621301775

For CC: 
Precision: 0.995004995004995, Recall: 0.996, F-score: 0.9955022488755622

For VBD: 
Precision: 0.9555288461538461, Recall: 0.9325513196480938, F-score: 0.943900267141585

For NN: 
Precision: 0.9502975148757438, Recall: 0.948471615720524, F-score: 0.9493836873852609

For WDT: 
Precision: 1.0, Recall: 0.569060773480663, F-score: 0.7253521126760564

For MD: 
Precision: 0.9715909090909091, Recall: 0.9941860465116279, F-score: 0.9827586206896551

For VB: 
Precision: 0.9109109109109109, Recall: 0.9257375381485249, F-score: 0.9182643794147326

For WRB: 
Precision: 1.0, Recall: 0.9659090909090909, F-score: 0.9826589595375723

For ,: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For VBG: 
Precision: 0.9367588932806324, Recall: 0.8061224489795918, F-score: 0.8665447897623401

For TO: 
Precision: 1.0, Recall: 1.0, F-score: 1.0

For POS: 
Precision: 0.9530201342281879, Recall: 0.9953271028037384, F-score: 0.9737142857142858

For .: 
Precision: 0.9994054696789536, Recall: 1.0, F-score: 0.999702646446625

For WP$: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For VBP: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For -LRB-: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For -RRB-: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For RB: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For VBN: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For VBZ: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For ``: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For '': 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For RBR: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For $: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For CD: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For JJR: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For RP: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For PRP: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For :: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For PRP$: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For PDT: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For RBS: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For WP: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For JJS: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For FW: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For LS: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For UH: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For EX: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For NNPS: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

For #: 
Precision: 0.0, Recall: 0.0, F-score: 0.0

Wrong classifications: 2083 / 40117

Percentage of correct classifications: 94.8076875140215 %

***






















