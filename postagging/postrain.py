import sys    
import os

def containsDigit(feature):
    for character in feature:
        if(character.isdigit()):
            return True
    return False

def getWordSign(feature):
    signature = ""
    prevChar = ""
    for character in feature:
        if(character.isupper() and prevChar != "A"):
            signature += "A"
            prevChar = "A"
        
        elif(character.islower() and prevChar != "a"):
            signature += "a"
            prevChar = "a"
        
        elif(character.isdigit() and prevChar != "9"):
            signature += "9"
            prevChar = "9"
        
        elif(character.isalpha()==False and character.isdigit()==False and prevChar != "-"):
            signature += "-"
            prevChar = "-"
            
    return signature

def formatFile(fileToBeFormatted, outputFilename):
    
    formattedFile = open(outputFilename, mode="w")
    
    #iterate over each sample
    for line in fileToBeFormatted:
        
        features = line.strip().split()
        
        for i in range(0, len(features)):
            
            tokenAndTag = features[i].split('/')  # eg. The/DT resulting/JJ firm/JJ prices/NNS and/CC stability/NN                
            
            if(len(tokenAndTag) != 2): #corrupted training data, token has not been formed properly in training data
                tokenAndTag = [features[i], "**undefined**"] #EXPERIMENTAL..PLEASE SEE THIS (tokenAndTag[1]) BEFORE SUBMITTING FINAL CODE!!!!!!!!!!!!!!!!!!!
          
            #token and its corresponding tag
            token = tokenAndTag[0]
            tag = tokenAndTag[1]    
            if(tag==""): #there simply HAS to be a tag present otherwise the classifier will not work
                tag = features[i]
            
            #******** find wprev, w and wnext, tprev and tnext ********
            #wprev and tprev
            if(i == 0):
                wprev = "**BegOfSent**"
                tprev = "**BegOfSent**"
            else:
                try:
                    wprev = features[i-1].split('/')[0]
                except: #format error in training data
                    wprev = "**undefined**"
                try:
                    tprev = features[i-1].split('/')[1]
                except: #format error in training data
                    tprev = "**undefined**"
            
            #w
            w = token
             
            #wnext and tnext
            if(i == len(features)-1):
                wnext = "**EndOfSent**"
            else:
                try:
                    wnext = features[i+1].split('/')[0]
                except: #format error in training data
                    wnext = "**undefined**"
            
            #write tag
            formattedFile.write(tag+" ")
            
            #******** write wprev, w, wnext, tprev & other features ********                
            #wprev
            formattedFile.write("wprev:{} ".format(wprev))
                
            #w
            formattedFile.write("w:{} ".format(w))
            
            #wnext
            formattedFile.write("wnext:{} ".format(wnext))
            
            #tprev
            if(tprev!="**BegOfSent**" and tprev!="**undefined**"):
                formattedFile.write("tprev:{} ".format(tprev))
            
            #lowercase token
            formattedFile.write("lcase:{} ".format(token.lower()))
            
            #1st letter capitlized or not
            if(len(token)>0):
                formattedFile.write("isCap:{} ".format(token[0].isupper()))
            
            #contains number    
            #formattedFile.write("containsNum:{} ".format(containsDigit(token)))
            
            #word signature   
            formattedFile.write("sign:{} ".format(getWordSign(token)))
            
            #prefix and suffix
            formattedFile.write("prefix:{} ".format(token[:2]))
            formattedFile.write("suffix:{} ".format(token[-3:]))
                
            #leave line
            formattedFile.write("\n")
    
    formattedFile.close()

def constructModel(trainingFilepath, modelFilepath, devFilepath):
    try:      
        #format training file
        trainingFile = open(trainingFilepath, mode="r", errors="ignore")
        formatFile(trainingFile, "posFormattedTrainingFile")   
        trainingFile.close()
        
        if(devFilepath != ""): #format dev file
            devFile = open(devFilepath, mode="r", errors="ignore")
            formatFile(devFile, "posFormattedDevFile") 
            devFile.close()
            
        #now call perceplearn
        if(devFilepath != ""):
            os.system("python3 ../perceplearn.py posFormattedTrainingFile "+modelFilepath+" -h posFormattedDevFile")
        else:
            os.system("python3 ../perceplearn.py posFormattedTrainingFile "+modelFilepath)
        
        #delete unwanted files
        os.remove("posFormattedTrainingFile")
        os.remove("posFormattedDevFile")
        
    except FileNotFoundError as e:
        print("File not found: {}".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 2):
        print("One or more arguments are missing. The correct syntax is \npython3 postrain.py <trainingFile> <modelFile> [-h <devFile>]")
    elif(numOfArgs > 4):
        print("Too many arguments. The correct syntax is \npython3 postrain.py <trainingFile> <modelFile> [-h <devFile>]")
    else:
        trainingFilepath = ""
        modelFilepath = ""
        devFilepath = ""
        
        indexOfh = -1
        
        try:
            indexOfh = sys.argv.index("-h")
        except ValueError:
            indexOfh = -1
        
        if(indexOfh == -1): #-h not provided
            if(numOfArgs > 2):
                print("Malformed command. The correct syntax is \npython3 postrain.py <trainingFile> <modelFile> [-h <devFile>]")          
                return                
            
            trainingFilepath = sys.argv[1]
            modelFilepath = sys.argv[2]
        else:
            try:
                if(indexOfh == 1):
                    devFilepath = sys.argv[2]
                    trainingFilepath = sys.argv[3]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 2):
                    devFilepath = sys.argv[3]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 3):
                    devFilepath = sys.argv[4]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[2]
            except IndexError:
                print("Malformed command. The correct syntax is \npython3 postrain.py <trainingFile> <modelFile> [-h <devFile>]")          
                return
        
        constructModel(trainingFilepath, modelFilepath, devFilepath)
    
if __name__ == "__main__":main()