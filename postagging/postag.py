import sys
import codecs

def containsDigit(feature):
    for character in feature:
        if(character.isdigit()):
            return True
    return False

def getWordSign(feature):
    signature = ""
    prevChar = ""
    for character in feature:
        if(character.isupper() and prevChar != "A"):
            signature += "A"
            prevChar = "A"
        
        elif(character.islower() and prevChar != "a"):
            signature += "a"
            prevChar = "a"
        
        elif(character.isdigit() and prevChar != "9"):
            signature += "9"
            prevChar = "9"
        
        elif(character.isalpha()==False and character.isdigit()==False and prevChar != "-"):
            signature += "-"
            prevChar = "-"
            
    return signature

def classify(modelFilepath):    
    try:  
        #******** READ MODEL FILE ********
        modelFile = open(modelFilepath, mode="r")
        
        lineNumber = 0.0
        
        #model parameters
        sortedClasses = []
        sizeOfVocab = 0.0
        avgWeightVectors = {}
        
        for line in modelFile:
            
            lineNumber += 1
            
            if(lineNumber == 1): #1st line in model file contains class names in sorted order
                sortedClasses = line.strip().split()
                
                #initialize weight vectors
                for className in sortedClasses:
                    avgWeightVectors[className] = {}
            
            elif(lineNumber == 2): #2nd line contains size of vocabulary
                sizeOfVocab = int(line.strip())
            
            else:
                parameters = line.strip().split()
                
                token = parameters[0]              
                
                i = 1
                for className in sortedClasses:
                    avgWeightVectors[className][token] = float(parameters[i])       
                    i += 1
        
        #finished reading model file
        modelFile.close()
                    
        if(len(avgWeightVectors[sortedClasses[0]].keys()) != sizeOfVocab):
            print("Model file is corrupted")
            return
        #*********************************
        
        
        #******** PERFORM POS TAGGING ON TEST DATA ********
        #start reading line by line from stdin
        sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore') #ignore any decoding errors
        inputLine = sys.stdin.readline()
        
        #EXPERIMENTAL
        #sampleFile = open("posClassifierResults", mode="w")
        #sampleFile = open("posFormattedTestFile", mode="w")
        
        while(len(inputLine.strip())>0): #Each line will be one sentence of tokenized data
            
            tokens = inputLine.strip().split() #test data will be pre-tokenized for this assignment
            
            tprev = "tprev:**BegOfSent**"
            
            for i in range(0, len(tokens)): #Iterate over each token and classify one token at a time
                
                z = {} #initialize z before classifying each token
                for className in sortedClasses:
                    z[className] = 0.0
                
                token = tokens[i]
                
                #**** Get hold of all features which describe the current token ****
                featureList = []
                
                #wprev
                if(i == 0):
                    wprev = "wprev:**BegOfSent**"
                else:
                    try:
                        wprev = "wprev:{}".format(tokens[i-1])
                    except: #format error in training data
                        wprev = "wprev:**undefined**"
                
                #w
                w = "w:"+token
                                 
                #wnext
                if(i == len(tokens)-1):
                    wnext = "wnext:**EndOfSent**"
                else:
                    try:
                        wnext = "wnext:{}".format(tokens[i+1])
                    except: #format error in training data
                        wnext = "wnext:**undefined**"
                
                #lowercase token
                lcase = "lcase:{}".format(token.lower())
                
                #1st letter capitlized or not
                if(len(token)>0):
                    isCap = "isCap:{}".format(token[0].isupper())
                
                #contains number
                #containsNum = "containsNum:{} ".format(containsDigit(token))
                
                #word signature   
                sign = "sign:{}".format(getWordSign(token))
                
                #prefix and suffix
                prefix = "prefix:{}".format(token[:2])
                suffix = "suffix:{}".format(token[-3:])
                    
                featureList.append(wprev)
                featureList.append(w)
                featureList.append(wnext)
                if(tprev!="tprev:**BegOfSent**"):
                    featureList.append(tprev)
                featureList.append(lcase)
                featureList.append(isCap)
                #featureList.append(containsNum)
                featureList.append(sign)
                featureList.append(prefix)
                featureList.append(suffix)
                #*********************************************************************
                    
                #Classify current token based on the above tokens
                for className in sortedClasses:
                    for feature in featureList:
                        if feature not in avgWeightVectors[className]: #not encountered during training
                            z[className] += 0.0
                        else:
                            z[className] += avgWeightVectors[className][feature]                
                            
                #Find Argmax
                z_pred_class = sortedClasses[0]
                z_max = 0.0
                for className in sortedClasses:
                    if(z[className] > z_max):
                        z_pred_class = className
                        z_max = z[className]
                        
                #EXPERIMENTAL            
                '''for feature in featureList:
                    sampleFile.write("{} ".format(feature))
                sampleFile.write("\n")'''
                #sampleFile.write("{}\n".format(z_pred_class))
                
                #Save prediction for current word to be used as part of feature set while predicting next word
                tprev = "tprev:{}".format(z_pred_class)
                
                #write output to stdout and flush it to output
                print("{}/{} ".format(token, z_pred_class), end="", flush=True)  
            
            #Leave a line
            print(flush=True)              
                
            #read in next line
            inputLine = sys.stdin.readline() 
            #*************************************************
        
        #EXPERIMENTAL
        #sampleFile.close()
        
    except FileNotFoundError as e:
        print("Unable to open file asd '{}'".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))      

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 1):
        print("One or more arguments are missing. The correct syntax is \npython3 postag.py <posModelFile>")
    elif(numOfArgs > 1):
        print("Too many arguments. The correct syntax is \npython3 postag.py <posModelFile>")
    else:
        modelFilepath = sys.argv[1] #0th position contains source file name.
        
        classify(modelFilepath)

if __name__ == '__main__': main()