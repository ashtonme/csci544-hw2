import sys
import codecs

def getTokenAndTags(features):
    tokenAndTags = features.split('/') # eg. El/DA Abogado/NC General/AQ del/SP Estado/NC                
            
    if(len(tokenAndTags) > 2): #token has not been formed properly in test data
        posTag = tokenAndTags[len(tokenAndTags)-1]
        token = features[:-1*(len(posTag)+1)] #features[:-1*(len(posTag)+1)] tokenAndTags[0]
    else:        
        token = tokenAndTags[0]
        posTag = tokenAndTags[1]
        
    return [token, posTag]            

def containsDigit(feature):
    for character in feature:
        if(character.isdigit()):
            return True
    return False

def getWordSign(feature):
    signature = ""
    prevChar = ""
    for character in feature:
        if(character.isupper() and prevChar != "A"):
            signature += "A"
            prevChar = "A"
        
        elif(character.islower() and prevChar != "a"):
            signature += "a"
            prevChar = "a"
        
        elif(character.isdigit() and prevChar != "9"):
            signature += "9"
            prevChar = "9"
        
        elif(character.isalpha()==False and character.isdigit()==False and prevChar != "-"):
            signature += "-"
            prevChar = "-"
            
    return signature

def classify(modelFilepath):    
    try:  
        #******** READ MODEL FILE ********
        modelFile = open(modelFilepath, mode="r")
        
        lineNumber = 0.0
        
        #model parameters
        sortedClasses = []
        sizeOfVocab = 0.0
        avgWeightVectors = {}
        
        for line in modelFile:
            
            lineNumber += 1
            
            if(lineNumber == 1): #1st line in model file contains class names in sorted order
                sortedClasses = line.strip().split()
                
                #initialize weight vectors
                for className in sortedClasses:
                    avgWeightVectors[className] = {}
            
            elif(lineNumber == 2): #2nd line contains size of vocabulary
                sizeOfVocab = int(line.strip())
            
            else:
                parameters = line.strip().split()
                
                token = parameters[0]              
                
                i = 1
                for className in sortedClasses:
                    avgWeightVectors[className][token] = float(parameters[i])       
                    i += 1
        
        #finished reading model file
        modelFile.close()
                    
        if(len(avgWeightVectors[sortedClasses[0]].keys()) != sizeOfVocab):
            print("Model file is corrupted")
            return
        #*********************************
        
        #******** PERFORM NER TAGGING ON TEST DATA ********
        #start reading line by line from stdin
        sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore') #ignore any decoding errors
        inputLine = sys.stdin.readline()
        
        #EXPERIMENTAL
        #sampleFile = open("nerClassifierResults", mode="w")
        #sampleFile = open("nerFormattedTestFile", mode="w")
        
        while(len(inputLine.strip())>0):
            
            tokens = inputLine.strip().split()
            
            nertprev = "nertprev:**BegOfSent**"
            
            for i in range(0, len(tokens)): #Iterate over each token and classify one token at a time
                
                z = {} #initialize z before classifying each token
                for className in sortedClasses:
                    z[className] = 0.0            
            
                #**** Get hold of all tokens which describe the current token ****
                featureList = []
                
                tokenAndTags = getTokenAndTags(tokens[i])
                        
                token = tokenAndTags[0]
                posTag = tokenAndTags[1]
                 
                #wprev and tprev
                if(i==0):
                    wprev = "wprev:**BegOfSent**"
                    tprev = "tprev:**BegOfSent**"
                else:
                    tokenAndTagsPrev = getTokenAndTags(tokens[i-1])                       
                    wprev = "wprev:{}".format(tokenAndTagsPrev[0])
                    tprev = "tprev:{}".format(tokenAndTagsPrev[1])
                
                #w
                w = "w:{}".format(token)
                
                #t
                t = "t:{}".format(posTag)
                
                #wnext and tnext
                if(i == len(tokens)-1):
                    wnext = "wnext:**EndOfSent**"
                    tnext = "tnext:**EndOfSent**"
                else:
                    tokenAndTagsNext = getTokenAndTags(tokens[i+1])        
                    wnext = "wnext:{}".format(tokenAndTagsNext[0])
                    tnext = "tnext:{}".format(tokenAndTagsNext[1])   
                
                #wpprev and tpprev
                if(i < 2):
                    wpprev = "wpprev:**BegOfSent**"
                    tpprev = "tpprev:**BegOfSent**"
                else:
                    tokenAndTagsPPrev = getTokenAndTags(tokens[i-2])                       
                    wpprev = "wpprev:{}".format(tokenAndTagsPPrev[0])
                    tpprev = "tpprev:{}".format(tokenAndTagsPPrev[1])
                    
                #wnnext and tnnext
                if(i >= len(tokens)-2):
                    wnnext = "wnnext:**EndOfSent**"
                    tnnext = "tnnext:**EndOfSent**"
                else:
                    tokenAndTagsNNext = getTokenAndTags(tokens[i+2])        
                    wnnext = "wnnext:{}".format(tokenAndTagsNNext[0])
                    tnnext = "tnnext:{}".format(tokenAndTagsNNext[1])
                
                #isCap
                if(len(token)>0):
                    isCap = "isCap:{}".format(token[0].isupper())
                
                #sign
                sign = "sign:{}".format(getWordSign(token))
            
                featureList.append(wprev)
                featureList.append(w)
                featureList.append(wnext)
                if(tprev!="tprev:**BegOfSent**"):
                    featureList.append(tprev)
                featureList.append(t)
                if(tnext!="tnext:**EndOfSent**"):
                    featureList.append(tnext)
                featureList.append(wpprev)
                featureList.append(wnnext)
                if(tpprev!="tpprev:**BegOfSent**"):
                    featureList.append(tpprev)
                if(tnnext!="tnnext:**EndOfSent**"):
                    featureList.append(tnnext)
                featureList.append(isCap)
                featureList.append(sign)
                if(nertprev != "**BegOfSent**"):
                    featureList.append(nertprev)
                #***********************************************************************************
                    
                #Classify current token based on the above tokens
                for className in sortedClasses:
                    for feature in featureList:
                        if feature not in avgWeightVectors[className]: #not encountered during training
                            z[className] += 0.0
                        else:
                            z[className] += avgWeightVectors[className][feature]                
                            
                #Find Argmax
                z_pred_class = sortedClasses[0]
                z_max = 0.0
                for className in sortedClasses:
                    if(z[className] > z_max):
                        z_pred_class = className
                        z_max = z[className]
                        
                #EXPERIMENTAL            
                '''for feature in featureList:
                    sampleFile.write("{} ".format(feature))
                sampleFile.write("\n")'''
                #sampleFile.write("{}\n".format(z_pred_class))
                
                #Save prediction for current word to be used as part of feature set while predicting next word
                nertprev = "nertprev:{}".format(z_pred_class)
                
                #write output to stdout and flush it to output
                print("{}/{}/{} ".format(token, posTag, z_pred_class), end="", flush=True)  
            
            #Leave a line
            print(flush=True)  
                
            #read in next line
            inputLine = sys.stdin.readline()     
        
        #EXPERIMENTAL
        #sampleFile.close()
        
    except FileNotFoundError as e:
        print("Unable to open file asd '{}'".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))     

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 1):
        print("One or more arguments are missing. The correct syntax is \npython3 netag.py <modelFile>")
    elif(numOfArgs > 1):
        print("Too many arguments. The correct syntax is \npython3 netag.py <modelFile>")
    else:
        modelFilepath = sys.argv[1] #0th position contains source file name.
        
        classify(modelFilepath)

if __name__ == '__main__': main()