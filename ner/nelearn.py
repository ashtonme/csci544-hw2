import sys    
import os

def getTokenAndTags(features):
    tokenAndTags = features.split('/') # eg. El/DA/O Abogado/NC/B-PER General/AQ/I-PER del/SP/I-PER Estado/NC/I-PER                
            
    if(len(tokenAndTags) > 3): #token has not been formed properly in training data
        posTag = tokenAndTags[len(tokenAndTags)-2]
        nerTag = tokenAndTags[len(tokenAndTags)-1]
        token = features[:-1*(len(posTag)+len(nerTag)+2)] # features[:-1*(len(posTag)+len(nerTag)+2)] tokenAndTags[0]
    else:        
        token = tokenAndTags[0]
        posTag = tokenAndTags[1]
        nerTag = tokenAndTags[2] 
        
    return [token, posTag, nerTag]

def getWordSign(feature):
    signature = ""
    prevChar = ""
    for character in feature:
        if(character.isupper() and prevChar != "A"):
            signature += "A"
            prevChar = "A"
        
        elif(character.islower() and prevChar != "a"):
            signature += "a"
            prevChar = "a"
        
        elif(character.isdigit() and prevChar != "9"):
            signature += "9"
            prevChar = "9"
        
        elif(character.isalpha()==False and character.isdigit()==False and prevChar != "-"):
            signature += "-"
            prevChar = "-"
            
    return signature

def containsDigit(feature):
    for character in feature:
        if(character.isdigit()):
            return True
    return False

def formatFile(fileToBeFormatted, outputFilename):
    
    formattedTestFile = open(outputFilename, mode="w")
    
    #iterate over each sample
    for line in fileToBeFormatted:
        
        features = line.strip().split()
        
        for i in range(0, len(features)):
            
            tokenAndTags = getTokenAndTags(features[i])
                    
            token = tokenAndTags[0]
            posTag = tokenAndTags[1]
            nerTag = tokenAndTags[2] 
            
            #write ner tag
            formattedTestFile.write(nerTag+" ")            
            
            #******** write wprev, w, wnext, tprev, t, tnext, nertprev, nert, nertnext ********  
            if(i==0):
                wprev = "**BegOfSent**"
                tprev = "**BegOfSent**"
                nertprev = "**BegOfSent**"
            else:
                tokenAndTagsPrev = getTokenAndTags(features[i-1])                       
                wprev = tokenAndTagsPrev[0]
                tprev = tokenAndTagsPrev[1]
                nertprev = tokenAndTagsPrev[2]
            if(i == len(features)-1):
                wnext = "**EndOfSent**"
                tnext = "**EndOfSent**"
            else:
                tokenAndTagsNext = getTokenAndTags(features[i+1])        
                wnext = tokenAndTagsNext[0]
                tnext = tokenAndTagsNext[1]                 
            
            #wprev
            formattedTestFile.write("wprev:{} ".format(wprev))
                
            #w
            formattedTestFile.write("w:{} ".format(token))
            
            #wnext
            formattedTestFile.write("wnext:{} ".format(wnext))
                
            #tprev
            if(tprev != "**BegOfSent**"):
                formattedTestFile.write("tprev:{} ".format(tprev))
                
            #t
            formattedTestFile.write("t:{} ".format(posTag))
            
            #tnext
            if(tnext != "**EndOfSent**"):
                formattedTestFile.write("tnext:{} ".format(tnext))
            
            #wpprev and tpprev
            if(i < 2):
                wpprev = "**BegOfSent**"
                tpprev = "**BegOfSent**"
            else:
                tokenAndTagsPPrev = getTokenAndTags(features[i-2])                       
                wpprev = tokenAndTagsPPrev[0]
                tpprev = tokenAndTagsPPrev[1]
                
            #wnnext and tnnext
            if(i >= len(features)-2):
                wnnext = "**EndOfSent**"
                tnnext = "**EndOfSent**"
            else:
                tokenAndTagsNNext = getTokenAndTags(features[i+2])        
                wnnext = tokenAndTagsNNext[0]
                tnnext = tokenAndTagsNNext[1]
                
            formattedTestFile.write("wpprev:{} ".format(wpprev))
            
            if(tpprev != "**BegOfSent**"):
                formattedTestFile.write("tpprev:{} ".format(tpprev))
                
            formattedTestFile.write("wnnext:{} ".format(wnnext))
            
            if(tnnext != "**EndOfSent**"):
                formattedTestFile.write("tnnext:{} ".format(tnnext))
            
            #isCap
            if(len(token)>0):
                formattedTestFile.write("isCap:{} ".format(token[0].isupper()))
                
            #sign
            formattedTestFile.write("sign:{} ".format(getWordSign(token)))
            
            #nertprev
            if(nertprev != "**BegOfSent**"):
                formattedTestFile.write("nertprev:{} ".format(nertprev))
                
            #leave line
            formattedTestFile.write("\n")
            
    formattedTestFile.close()

def constructModel(trainingFilepath, modelFilepath, devFilepath):
    try:        
        #format training file
        trainingFile = open(trainingFilepath, mode="r", errors="ignore")      
        formatFile(trainingFile, "neFormattedTrainingFile") 
        trainingFile.close()
        
        if(devFilepath != ""): #format dev file
            devFile = open(devFilepath, mode="r", errors="ignore")
            formatFile(devFile, "neFormattedDevFile") 
            devFile.close()
            
        #now call perceplearn
        if(devFilepath != ""):
            os.system("python3 ../perceplearn.py neFormattedTrainingFile "+modelFilepath+" -h neFormattedDevFile")
        else:
            os.system("python3 ../perceplearn.py neFormattedTrainingFile "+modelFilepath)
        
        #delete unwanted files
        os.remove("neFormattedTrainingFile")
        os.remove("neFormattedDevFile")
            
    except FileNotFoundError as e:
        print("File not found: {}".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 2):
        print("One or more arguments are missing. The correct syntax is \npython3 nelearn.py <trainingFile> <modelFile> [-h <devFile>]")
    elif(numOfArgs > 4):
        print("Too many arguments. The correct syntax is \npython3 nelearn.py <trainingFile> <modelFile> [-h <devFile>]")
    else:
        trainingFilepath = ""
        modelFilepath = ""
        devFilepath = ""
        
        indexOfh = -1
        
        try:
            indexOfh = sys.argv.index("-h")
        except ValueError:
            indexOfh = -1
        
        if(indexOfh == -1): #-h not provided
            if(numOfArgs > 2):
                print("Malformed command. The correct syntax is \npython3 nelearn.py <trainingFile> <modelFile> [-h <devFile>]")          
                return                
            
            trainingFilepath = sys.argv[1]
            modelFilepath = sys.argv[2]
        else:
            try:
                if(indexOfh == 1):
                    devFilepath = sys.argv[2]
                    trainingFilepath = sys.argv[3]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 2):
                    devFilepath = sys.argv[3]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[4]
                elif(indexOfh == 3):
                    devFilepath = sys.argv[4]
                    trainingFilepath = sys.argv[1]
                    modelFilepath = sys.argv[2]
            except IndexError:
                print("Malformed command. The correct syntax is \npython3 nelearn.py <trainingFile> <modelFile> [-h <devFile>]")          
                return
        
        constructModel(trainingFilepath, modelFilepath, devFilepath)
    
if __name__ == "__main__":main()